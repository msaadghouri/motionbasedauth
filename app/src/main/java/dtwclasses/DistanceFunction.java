package dtwclasses;

/**
 * Created by Mohammad-Ghouri on 3/2/16.
 */
public interface DistanceFunction {

    public double calcDistance(double[] vector1, double[] vector2);

}
