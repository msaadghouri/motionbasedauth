package model;

/**
 * Created by Mohammad-Ghouri on 7/14/16.
 */


import java.util.ArrayList;

public class MathCalUtils {

    private MathCalUtils() {
    }

    public static double getSum(ArrayList<Double> arrayList) {
        double sum = 0;
        for (double d : arrayList) {
            sum = sum + d;
        }
        return sum;
    }

    public static double getMean(ArrayList<Double> arrayList) {
        double sum = getSum(arrayList);
        double mean = sum / arrayList.size();
        return mean;
    }

    public static double getStd(ArrayList<Double> arrayList) {
        double sqsum = 0;
        double mean = getMean(arrayList);
        for (double d : arrayList) {
            sqsum = sqsum + Math.pow((d - mean), 2);
        }
        double std = Math.sqrt(sqsum / (arrayList.size() - 1));
        return std;
    }
}

