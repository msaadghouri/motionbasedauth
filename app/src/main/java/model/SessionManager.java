package model;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Mohammad-Ghouri on 3/16/16.
 */
public class SessionManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;

    public static final String PREF_NAME = "Threshold";
    public static final String smartThreshold = "smartThreshold";
    private static final String IS_FIRST_TIME_LAUNCH = "FirstLaunch";
    private static final String HAS_TRAINING_COMPLETED = "TrainingCompleted";
    public static final String DATE_NUMBER = "Datenumber";
    public static final String LEFT_COUNT = "Leftcount";
    public static final String CURRENT_TEST_COUNT = "Testcount";

    public SessionManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void saveThresholdValue(int threshold) {

        editor.putInt(smartThreshold, threshold);
        editor.commit();
    }

    public void setFirstLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }

    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }

    public void setTrainingCompleted(boolean isFirstTime) {
        editor.putBoolean(HAS_TRAINING_COMPLETED, isFirstTime);
        editor.commit();
    }

    public boolean hasTrainingCompleted() {
        return pref.getBoolean(HAS_TRAINING_COMPLETED, true);
    }

    public void saveTestDateAndCount(int dateNumber, int leftCount, int testCount) {
        editor.putInt(DATE_NUMBER, dateNumber);
        editor.putInt(LEFT_COUNT, leftCount);
        editor.putInt(CURRENT_TEST_COUNT, testCount);
        editor.commit();

    }
}
