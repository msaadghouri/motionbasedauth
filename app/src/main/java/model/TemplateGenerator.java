package model;

import java.io.File;
import java.util.ArrayList;

import dtwclasses.DTW;
import dtwclasses.DistanceFunction;
import dtwclasses.DistanceFunctionFactory;
import dtwclasses.TimeWarpInfo;
import dtwclasses.TimeSeries;



public class TemplateGenerator {

    private TemplateGenerator() {}

    public static ArrayList<Double> generateMeanDistList(String trainDataDir) {
        ArrayList<Double> meanDistList = new ArrayList<>();
        int trainSize = new File(trainDataDir).listFiles().length;
        for (int i = 0; i < trainSize; i++) {
            ArrayList<Double> distList = generateDistList(trainDataDir, trainSize, i);
            double mean = calArrayListMean(distList);
            meanDistList.add(mean);
        }
        return meanDistList;
    }

    public static ArrayList<Double> generateDistList(String trainDataDir, int trainSize, int index) {
        ArrayList<Double> distList = new ArrayList<>();
        String mTrainA = trainDataDir + "train" + String.valueOf(index + 1) + ".txt";
        for (int j = 0; j < trainSize; j++) {
            String mTrainB = trainDataDir + "train" + String.valueOf(j + 1) + ".txt";
            if (index == j) {
                distList.add((double) 0);
            } else {
                distList.add(DtwDistance(mTrainA, mTrainB));
            }
        }
        return distList;
    }

    private static double calArrayListMean(ArrayList<Double> distList) {
        double sum = 0;
        for (double dist : distList) {
            sum += dist;
        }
        return sum / (distList.size() - 1);
    }
    public static double DtwDistance(String dataFile1, String dataFile2) {
        TimeSeries tsI = new TimeSeries(dataFile1, true, false, ',');
        TimeSeries tsJ = new TimeSeries(dataFile2, true, false, ',');
        final DistanceFunction distFn;
        distFn = DistanceFunctionFactory.getDistFnByName("EuclideanDistance");
        final TimeWarpInfo info = DTW.getWarpInfoBetween(tsI, tsJ, distFn);
        return info.getDistance();
    }

}
