package model;

import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileSaveUtils {

    private FileSaveUtils() {
    }

    public static boolean checkStorageStatus() {
        boolean result;
        String storageState = Environment.getExternalStorageState();
        if (storageState.equals(Environment.MEDIA_MOUNTED)) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    public static void save(String filepath, String data) {
        File file = new File(filepath);
        save(file, data);
    }

    public static void save(File file, String data) {
        File fileDir = file.getParentFile();
        if (!fileDir.exists()) {
            fileDir.mkdirs();
        }
        try {
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(data.getBytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}