package model;

/**
 * Created by Mohammad-Ghouri on 7/13/16.
 */
public class AllSensorData {
    private long timestamp;
    private double axCoordinate;
    private double ayCoordinate;
    private double azCoordinate;
    private double gxCoordinate;
    private double gyCoordinate;
    private double gzCoordinate;
    private double mxCoordinate;
    private double myCoordinate;
    private double mzCoordinate;
    private double oxCoordinate;
    private double oyCoordinate;
    private double ozCoordinate;

    public AllSensorData(long timestamp,
                         double axCoordinate, double ayCoordinate, double azCoordinate,
                         double gxCoordinate, double gyCoordinate, double gzCoordinate,
                         double mxCoordinate, double myCoordinate, double mzCoordinate,
                         double oxCoordinate, double oyCoordinate, double ozCoordinate) {
        this.timestamp = timestamp;
        this.axCoordinate = axCoordinate;
        this.ayCoordinate = ayCoordinate;
        this.azCoordinate = azCoordinate;
        this.gxCoordinate = gxCoordinate;
        this.gyCoordinate = gyCoordinate;
        this.gzCoordinate = gzCoordinate;
        this.mxCoordinate = mxCoordinate;
        this.myCoordinate = myCoordinate;
        this.mzCoordinate = mzCoordinate;
        this.oxCoordinate = oxCoordinate;
        this.oyCoordinate = oyCoordinate;
        this.ozCoordinate = ozCoordinate;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public double getAxCoordinate() {
        return axCoordinate;
    }

    public void setAxCoordinate(double axCoordinate) {
        this.axCoordinate = axCoordinate;
    }

    public double getAyCoordinate() {
        return ayCoordinate;
    }

    public void setAyCoordinate(double ayCoordinate) {
        this.ayCoordinate = ayCoordinate;
    }

    public double getAzCoordinate() {
        return azCoordinate;
    }

    public void setAzCoordinate(double azCoordinate) {
        this.azCoordinate = azCoordinate;
    }

    public double getGxCoordinate() {
        return gxCoordinate;
    }

    public void setGxCoordinate(double gxCoordinate) {
        this.gxCoordinate = gxCoordinate;
    }

    public double getGyCoordinate() {
        return gyCoordinate;
    }

    public void setGyCoordinate(double gyCoordinate) {
        this.gyCoordinate = gyCoordinate;
    }

    public double getGzCoordinate() {
        return gzCoordinate;
    }

    public void setGzCoordinate(double gzCoordinate) {
        this.gzCoordinate = gzCoordinate;
    }

    public double getMxCoordinate() {
        return mxCoordinate;
    }

    public void setMxCoordinate(double mxCoordinate) {
        this.mxCoordinate = mxCoordinate;
    }

    public double getMyCoordinate() {
        return myCoordinate;
    }

    public void setMyCoordinate(double myCoordinate) {
        this.myCoordinate = myCoordinate;
    }

    public double getMzCoordinate() {
        return mzCoordinate;
    }

    public void setMzCoordinate(double mzCoordinate) {
        this.mzCoordinate = mzCoordinate;
    }

    public double getOxCoordinate() {
        return oxCoordinate;
    }

    public void setOxCoordinate(double oxCoordinate) {
        this.oxCoordinate = oxCoordinate;
    }

    public double getOyCoordinate() {
        return oyCoordinate;
    }

    public void setOyCoordinate(double oyCoordinate) {
        this.oyCoordinate = oyCoordinate;
    }

    public double getOzCoordinate() {
        return ozCoordinate;
    }

    public void setOzCoordinate(double ozCoordinate) {
        this.ozCoordinate = ozCoordinate;
    }

    @Override
    public String toString() {
        return "timestamp=" + timestamp +
                        ", axCoordinate=" + axCoordinate +
                        ", ayCoordinate=" + ayCoordinate +
                        ", azCoordinate=" + azCoordinate +
                        ", gxCoordinate=" + gxCoordinate +
                        ", gyCoordinate=" + gyCoordinate +
                        ", gzCoordinate=" + gzCoordinate +
                        ", mxCoordinate=" + mxCoordinate +
                        ", myCoordinate=" + myCoordinate +
                        ", mzCoordinate=" + mzCoordinate +
                        ", oxCoordinate=" + oxCoordinate +
                        ", oyCoordinate=" + oyCoordinate +
                        ", ozCoordinate=" + ozCoordinate;
    }
}
