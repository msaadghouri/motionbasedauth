package com.smartapps.main;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import model.SessionManager;

/**
 * Created by Mohammad-Ghouri on 6/29/16.
 */
public class VideoClass extends Activity {
    private VideoView mediaPlayer;
    private Button playButton, sliderPlay;
    private DownloadClass dClass;
    private ProgressDialog PD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().hide();
        setContentView(R.layout.video_layout);
        initGUI();
        dClass = new DownloadClass();
        playButton.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View v) {

                File videoFile = new File(getApplicationContext().getExternalFilesDir(null),
                        "u" + String.valueOf(MenuActivity.userID) + "/video/Video.mp4");
                if (videoFile.exists()) {
                    String uriPath = getApplicationContext().getExternalFilesDir(null) +
                            "/u" + String.valueOf(MenuActivity.userID) + "/video/Video.mp4";
                    Uri uri = Uri.parse(uriPath);
                    mediaPlayer.setVideoURI(uri);
                    mediaPlayer.requestFocus();
                    mediaPlayer.start();
                } else {
                    if (connectivity()) {
                        dClass.execute("");
                    } else {
                        Toast.makeText(getApplication(), "No Internet Connection", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });
        sliderPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionManager sessionManager = new SessionManager(getApplicationContext());
                sessionManager.setFirstLaunch(true);
                startActivity(new Intent(VideoClass.this, SliderActivity.class));
                finish();
            }
        });

    }

    private void initGUI() {
        mediaPlayer = (VideoView) findViewById(R.id.videoview);
        playButton = (Button) findViewById(R.id.playButton);
        sliderPlay = (Button) findViewById(R.id.playSlider);
    }

    class DownloadClass extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            PD = ProgressDialog.show(VideoClass.this, null, "Please Wait... Downloading!", true);
            PD.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... arg0) {
            DownloadFile("http://52.37.96.249/user1/u1_2dpattern.mp4", "Video.mp4");
            return null;
        }
    }

    public void DownloadFile(String fileURL, String fileName) {
        try {
            String storageState = Environment.getExternalStorageState();
            if (storageState.equals(Environment.MEDIA_MOUNTED)) {
                File dirVideo = new File(this.getExternalFilesDir(null), "u" + String.valueOf(MenuActivity.userID) + "/video");

                if (!dirVideo.exists())
                    dirVideo.mkdirs();

                URL url = new URL(fileURL);
                HttpURLConnection httpGet = (HttpURLConnection) url.openConnection();
                httpGet.setRequestMethod("GET");
                httpGet.setDoOutput(true);
                httpGet.connect();
                FileOutputStream fOS = new FileOutputStream(new File(dirVideo, fileName));
                InputStream in = httpGet.getInputStream();
                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = in.read(buffer)) > 0) {
                    fOS.write(buffer, 0, len1);
                }
                fOS.close();
                PD.dismiss();
            } else {
                Log.i("video saving problem", "unable to write to external storage");
            }
        } catch (Exception e) {
            Log.d("Error : ", e.toString());
        }
    }


    private boolean connectivity() {
        ConnectivityManager connectivity = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if (info != null) {
                if (info.getState() == NetworkInfo.State.CONNECTED) {
                    return true;
                }

            }
        }
        return false;
    }
}


