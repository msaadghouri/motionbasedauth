package com.smartapps.main;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.TextView;

import model.SessionManager;

/**
 * Created by Mohammad-Ghouri on 6/29/16.
 */
public class SummaryClass extends Activity {
    private TextView day1, day2, day3, day4, day5;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().hide();
        setContentView(R.layout.newsummary);
        initGUI();
        preferences = getSharedPreferences(SessionManager.PREF_NAME, MODE_PRIVATE);


        if (preferences.getInt(SessionManager.DATE_NUMBER, 0) == 1) {
            day1.setText(String.valueOf(preferences.getInt(SessionManager.LEFT_COUNT, 0)));
            if (preferences.getInt(SessionManager.LEFT_COUNT, 0) > 0) {
                day1.setTextColor(getResources().getColor(R.color.activecolor, null));
            } else {
                day1.setTextColor(getResources().getColor(R.color.compeletedcolor, null));
            }
        } else if (preferences.getInt(SessionManager.DATE_NUMBER, 0) == 2) {
            day1.setText("0");
            day1.setTextColor(getResources().getColor(R.color.compeletedcolor, null));
            day2.setText(String.valueOf(preferences.getInt(SessionManager.LEFT_COUNT, 0)));
            if (preferences.getInt(SessionManager.LEFT_COUNT, 0) > 0) {
                day2.setTextColor(getResources().getColor(R.color.activecolor, null));
            } else {
                day2.setTextColor(getResources().getColor(R.color.compeletedcolor, null));
            }
        } else if (preferences.getInt(SessionManager.DATE_NUMBER, 0) == 3) {
            day1.setText("0");
            day1.setTextColor(getResources().getColor(R.color.compeletedcolor, null));
            day2.setText("0");
            day2.setTextColor(getResources().getColor(R.color.compeletedcolor, null));
            day3.setText(String.valueOf(preferences.getInt(SessionManager.LEFT_COUNT, 0)));
            if (preferences.getInt(SessionManager.LEFT_COUNT, 0) > 0) {
                day3.setTextColor(getResources().getColor(R.color.activecolor, null));
            } else {
                day3.setTextColor(getResources().getColor(R.color.compeletedcolor, null));
            }
        } else if (preferences.getInt(SessionManager.DATE_NUMBER, 0) == 4) {
            day1.setText("0");
            day1.setTextColor(getResources().getColor(R.color.compeletedcolor, null));
            day2.setText("0");
            day2.setTextColor(getResources().getColor(R.color.compeletedcolor, null));
            day3.setText("0");
            day3.setTextColor(getResources().getColor(R.color.compeletedcolor, null));
            day4.setText(String.valueOf(preferences.getInt(SessionManager.LEFT_COUNT, 0)));
            if (preferences.getInt(SessionManager.LEFT_COUNT, 0) > 0) {
                day4.setTextColor(getResources().getColor(R.color.activecolor, null));
            } else {
                day4.setTextColor(getResources().getColor(R.color.compeletedcolor, null));
            }
        } else if (preferences.getInt(SessionManager.DATE_NUMBER, 0) == 5) {
            day1.setText("0");
            day1.setTextColor(getResources().getColor(R.color.compeletedcolor, null));
            day2.setText("0");
            day2.setTextColor(getResources().getColor(R.color.compeletedcolor, null));
            day3.setText("0");
            day3.setTextColor(getResources().getColor(R.color.compeletedcolor, null));
            day4.setText("0");
            day4.setTextColor(getResources().getColor(R.color.compeletedcolor, null));
            day5.setText(String.valueOf(preferences.getInt(SessionManager.LEFT_COUNT, 0)));
            if (preferences.getInt(SessionManager.LEFT_COUNT, 0) > 0) {
                day5.setTextColor(getResources().getColor(R.color.activecolor, null));
            } else {
                day5.setTextColor(getResources().getColor(R.color.compeletedcolor, null));
            }
        }


    }

    private void initGUI() {
        day1 = (TextView) findViewById(R.id.day1counter);
        day2 = (TextView) findViewById(R.id.day2counter);
        day3 = (TextView) findViewById(R.id.day3counter);
        day4 = (TextView) findViewById(R.id.day4counter);
        day5 = (TextView) findViewById(R.id.day5counter);

    }
}
