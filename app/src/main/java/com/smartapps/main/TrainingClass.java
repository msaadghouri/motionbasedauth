package com.smartapps.main;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import model.AccelerometerData;
import model.AllSensorData;
import model.FileSaveUtils;
import model.IndexValuePair;
import model.MathCalUtils;
import model.SessionManager;
import model.TemplateGenerator;

/**
 * Created by Mohammad-Ghouri on 6/29/16.
 */
public class TrainingClass extends Activity implements SensorEventListener {
    private LinearLayout gestureLayout;
    private ImageButton recordStatus;
    private TextView trainingCount;
    private Button proceedButton, finishButton;
    private SharedPreferences prefs;
    private SessionManager sessionManager;
    private boolean started = false;
    private SensorManager sensorManager;
    AccelerometerData data;
    private ArrayList<AccelerometerData> sensorData;
    private int counter = 30, fileNameCounter = 1;
    private ProgressDialog PD;
    private BackGroundClass bClass;

    private AllSensorData allSensorData;
    private ArrayList<AllSensorData> allSensorDataArrayList;
    private double ax, ay, az, gx, gy, gz, mx, my, mz, ox, oy, oz;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().hide();
        setContentView(R.layout.training_layout);
        initGUI();

        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sessionManager = new SessionManager(getApplicationContext());
        prefs = getSharedPreferences(SessionManager.PREF_NAME, MODE_PRIVATE);
        sensorData = new ArrayList<>();
        allSensorDataArrayList = new ArrayList<>();
        bClass = new BackGroundClass();

        gestureLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    recordStatus.setBackgroundResource(R.drawable.circel_green);
                    sensorData = new ArrayList<>();
                    allSensorDataArrayList = new ArrayList<>();
                    started = true;
                    sensorManager.registerListener(TrainingClass.this,
                            sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST);
//                    sensorManager.registerListener(TrainingClass.this,
//                            sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_FASTEST);
//                    sensorManager.registerListener(TrainingClass.this,
//                            sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD), SensorManager.SENSOR_DELAY_FASTEST);
//                    sensorManager.registerListener(TrainingClass.this,
//                            sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_FASTEST);


                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    recordStatus.setBackgroundResource(R.drawable.circle_red);
                    started = false;
                    sensorManager.unregisterListener(TrainingClass.this);
                }

                return true;
            }
        });


        proceedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter--;
                if (sensorData.size() > 0) {
                    trainingCount.setText("" + counter);
                    savingSensorData(sensorData, fileNameCounter);
                    //savingAllSensorData(allSensorDataArrayList, fileNameCounter);
                    fileNameCounter++;

                } else {
                    counter++;
                    Toast.makeText(getApplicationContext(), "No Recorded Gesture", Toast.LENGTH_SHORT).show();
                }
                if (counter == 0) {
                    gestureLayout.setEnabled(false);
                    proceedButton.setVisibility(View.GONE);
                    finishButton.setVisibility(View.VISIBLE);
                }

            }

        });
        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bClass.execute("");
            }
        });

    }


    @Override
    public void onBackPressed() {
        showAlertDialog("Going back may result in starting the training again");
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
    }

    class BackGroundClass extends AsyncTask<String, String, String> {


        @Override
        protected void onPreExecute() {
            PD = ProgressDialog.show(TrainingClass.this, null, "Please Wait...", true);
            PD.setCancelable(false);
        }

        @Override
        protected String doInBackground(String... arg0) {
            generateTemplate();
            return null;
        }
    }

    private void savingSensorData(ArrayList<AccelerometerData> sData, int fileCount) {
        StringBuilder sBuilder = new StringBuilder();
        if (sData.size() > 0) {
            for (int i = 0; i < sData.size(); i++) {
                sBuilder.append(sData.get(i).getX() + " " + sData.get(i).getY() + " " + sData.get(i).getZ() + "\n");
            }

            try {
                String storageState = Environment.getExternalStorageState();
                if (storageState.equals(Environment.MEDIA_MOUNTED)) {
                    File dirTrain = new File(this.getExternalFilesDir(null), "u" + String.valueOf(MenuActivity.userID) + "/train");

                    if (!dirTrain.exists()) {
                        dirTrain.mkdirs();
                        Log.i("Directory", "Created");
                    } else if (dirTrain.exists()) {
                        Log.i("Directory", "Exists");
                    }
                    File mFile = new File(dirTrain, "train" + String.valueOf(fileCount) + ".txt");
                    FileOutputStream fos = new FileOutputStream(mFile);
                    fos.write(sBuilder.toString().getBytes());
                    fos.close();
                } else {
                    Log.i("file saving problem", "unable to write to external storage");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            sData.clear();
        } else {
            Toast.makeText(this, "No Recorded Gesture", Toast.LENGTH_SHORT).show();
        }

    }


    private void initGUI() {
        gestureLayout = (LinearLayout) findViewById(R.id.toTouchLayout);
        recordStatus = (ImageButton) findViewById(R.id.btnRecordStatus);
        trainingCount = (TextView) findViewById(R.id.trainingCounter);
        proceedButton = (Button) findViewById(R.id.proceedButton);
        finishButton = (Button) findViewById(R.id.btnRetrieve);
    }

    protected boolean generateTemplate() {
        String mTrainDataDir = this.getExternalFilesDir(null) + "/u" + String.valueOf(MenuActivity.userID) + "/train/";
        ArrayList<Double> meanDistList = TemplateGenerator.generateMeanDistList(mTrainDataDir);
        IndexValuePair[] meanDistPairArray = new IndexValuePair[meanDistList.size()];
        for (int i = 0; i < meanDistList.size(); i++) {
            meanDistPairArray[i] = new IndexValuePair(i, meanDistList.get(i));
        }
        Arrays.sort(meanDistPairArray, new Comparator<IndexValuePair>() {
            @Override
            public int compare(IndexValuePair m, IndexValuePair n) {
                return Double.compare(m.getValue(), n.getValue());
            }
        });
        int templateNum = 5;
        double[] templateDists = new double[templateNum];
        String mTemplateDataDir = this.getExternalFilesDir(null) + "/u" + String.valueOf(MenuActivity.userID) + "/template/";
        for (int i = 0; i < templateNum; i++) {
            templateDists[i] = meanDistPairArray[i].getValue();
            int trainNum = meanDistPairArray[i].getIndex() + 1;
            String sTrainPath = mTrainDataDir + "train" + String.valueOf(trainNum) + ".txt";
            File sTrainFile = new File(sTrainPath);
            String dTemplatePath = mTemplateDataDir + "template" + String.valueOf(i + 1) + ".txt";
            File dTemplateFile = new File(dTemplatePath);
            try {
                FileUtils.copyFile(sTrainFile, dTemplateFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        saveThreshold(meanDistList);
        return true;
    }

//    protected boolean generateTemplate() {
//        String mTrainDataDir = this.getExternalFilesDir(null) + "/u" + String.valueOf(MenuActivity.userID) + "/train/";
//        ArrayList<Double> meanDistList = TemplateGenerator.generateMeanDistList(mTrainDataDir);
//        IndexValuePair[] meanDistPairArray = new IndexValuePair[meanDistList.size()];
//        for (int i = 0; i < meanDistList.size(); i++) {
//            meanDistPairArray[i] = new IndexValuePair(i, meanDistList.get(i));
//        }
//        Arrays.sort(meanDistPairArray, new Comparator<IndexValuePair>() {
//            @Override
//            public int compare(IndexValuePair m, IndexValuePair n) {
//                return Double.compare(m.getValue(), n.getValue());
//            }
//        });
//        int templateNum = 5;
//        double[] templateDists = new double[templateNum];
//        String mTemplateDataDir = this.getExternalFilesDir(null) + "/u" + String.valueOf(MenuActivity.userID) + "/template/";
//        for (int i = 0; i < templateNum; i++) {
//            templateDists[i] = meanDistPairArray[i].getValue();
//            int trainNum = meanDistPairArray[i].getIndex() + 1;
//            String sTrainPath = mTrainDataDir + "train" + String.valueOf(trainNum) + ".csv";
//            File sTrainFile = new File(sTrainPath);
//            String dTemplatePath = mTemplateDataDir + "template" + String.valueOf(i + 1) + ".csv";
//            File dTemplateFile = new File(dTemplatePath);
//            try {
//                FileUtils.copyFile(sTrainFile, dTemplateFile);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        saveThreshold(meanDistList);
//        return true;
//    }

    protected void saveThreshold(ArrayList<Double> meanDistList) {
        double mean = MathCalUtils.getMean(meanDistList);
        double std = MathCalUtils.getStd(meanDistList);
        // use u+3o as the maximum boundary - threshold
        double threshold = mean + 3 * std;
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong("threshold", Double.doubleToRawLongBits(threshold));
        editor.apply();
        String thresPath = this.getExternalFilesDir(null) + "/u" + String.valueOf(MenuActivity.userID) + "/threshold.txt";
        FileSaveUtils.save(thresPath, String.valueOf(threshold));
        // Output all 30 meanDists to meandists.txt
        StringBuilder stringBuilder = new StringBuilder();
        for (double d : meanDistList) {
            stringBuilder.append(d).append("\n");
        }
        String meanDists = stringBuilder.toString();
        String distFilePath = this.getExternalFilesDir(null) + "/u" + String.valueOf(MenuActivity.userID) + "/meandists.txt";
        FileSaveUtils.save(distFilePath, meanDists);

        PD.dismiss();
        SessionManager sessionManager = new SessionManager(getApplicationContext());
        sessionManager.setTrainingCompleted(false);
        setResult(RESULT_OK);
        finish();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (started) {

            double x = event.values[0];
            double y = event.values[1];
            double z = event.values[2];

            long timestamp = System.currentTimeMillis();
            data = new AccelerometerData(timestamp, x, y, z);
            sensorData.add(data);

        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    protected void showAlertDialog(String string) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

        alertDialog.setIcon(R.drawable.logonew);
        alertDialog.setMessage(string);
        alertDialog.setPositiveButton("Stay",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

        alertDialog.setNegativeButton("Leave",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        startActivity(new Intent(TrainingClass.this, MenuActivity.class));
                        finish();
                    }
                });

        alertDialog.show();

    }
    //    protected boolean generateTemplate() {
//        String mTrainDataDir = this.getExternalFilesDir(null) + "/u" + String.valueOf(MenuActivity.userID) + "/train/";
//        ArrayList<Double> meanDistList = TemplateGenerator.generateMeanDistList(mTrainDataDir);
//        IndexValuePair[] meanDistPairArray = new IndexValuePair[meanDistList.size()];
//        for (int i = 0; i < meanDistList.size(); i++) {
//            meanDistPairArray[i] = new IndexValuePair(i, meanDistList.get(i));
//        }
//        Arrays.sort(meanDistPairArray, new Comparator<IndexValuePair>() {
//            @Override
//            public int compare(IndexValuePair m, IndexValuePair n) {
//                return Double.compare(m.getValue(), n.getValue());
//            }
//        });
//        int templateNum = 5;
//        double[] templateDists = new double[templateNum];
//        String mTemplateDataDir = this.getExternalFilesDir(null) + "/u" + String.valueOf(MenuActivity.userID) + "/template/";
//        for (int i = 0; i < templateNum; i++) {
//            templateDists[i] = meanDistPairArray[i].getValue();
//            int trainNum = meanDistPairArray[i].getIndex() + 1;
//            String sTrainPath = mTrainDataDir + "train" + String.valueOf(trainNum) + ".txt";
//            File sTrainFile = new File(sTrainPath);
//            String dTemplatePath = mTemplateDataDir + "template" + String.valueOf(i + 1) + ".txt";
//            File dTemplateFile = new File(dTemplatePath);
//            try {
//                FileUtils.copyFile(sTrainFile, dTemplateFile);
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        saveThreshold(templateDists);
//        return true;
//    }
//
//    private void savingAllSensorData(ArrayList<AllSensorData> getall, int fileNameCounter) {
//        StringBuilder sBuilder = new StringBuilder();
//        if (getall.size() > 0) {
//            for (int i = 0; i < getall.size(); i++) {
//                sBuilder.append(getall.get(i).getAxCoordinate() + " " + getall.get(i).getAyCoordinate() + " " + getall.get(i).getAzCoordinate()
//                        + " " + getall.get(i).getGxCoordinate() + " " + getall.get(i).getGyCoordinate() + " " + getall.get(i).getGzCoordinate()
//                        + " " + getall.get(i).getMxCoordinate() + " " + getall.get(i).getMyCoordinate() + " " + getall.get(i).getMzCoordinate()
//                        + " " + getall.get(i).getOxCoordinate() + " " + getall.get(i).getOyCoordinate() + " " + getall.get(i).getOzCoordinate()
//                        + "\n");
//            }
//
//            try {
//                String storageState = Environment.getExternalStorageState();
//                if (storageState.equals(Environment.MEDIA_MOUNTED)) {
//                    File dirTrain = new File(this.getExternalFilesDir(null), "u" + String.valueOf(MenuActivity.userID) + "/allsensordata");
//
//                    if (!dirTrain.exists()) {
//                        dirTrain.mkdirs();
//                        Log.i("Directory", "Created");
//                    } else if (dirTrain.exists()) {
//                        Log.i("Directory", "Exists");
//                    }
//                    File mFile = new File(dirTrain, "allsensortrain" + String.valueOf(fileNameCounter) + ".txt");
//                    FileOutputStream fos = new FileOutputStream(mFile);
//                    fos.write(sBuilder.toString().getBytes());
//                    fos.close();
//                } else {
//                    Log.i("file saving problem", "unable to write to external storage");
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            getall.clear();
//        } else {
//            Toast.makeText(this, "No Recorded Gesture", Toast.LENGTH_SHORT).show();
//        }
//
//    }
    //    protected void saveThreshold(double[] templateDists) {
//        double distSum = 0;
//        for (double dist : templateDists) {
//            distSum += dist;
//        }
//        double threshold = distSum / templateDists.length;
//        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
//        SharedPreferences.Editor editor = prefs.edit();
//        editor.putLong("threshold", Double.doubleToRawLongBits(threshold));
//        editor.apply();
//        String thresPath = this.getExternalFilesDir(null) + "/u" + String.valueOf(MenuActivity.userID) + "/threshold.txt";
//        try {
//            FileOutputStream fos = new FileOutputStream(new File(thresPath));
//            fos.write(String.valueOf(threshold).getBytes());
//            fos.close();
//            PD.dismiss();
//            SessionManager sessionManager = new SessionManager(getApplicationContext());
//            sessionManager.setTrainingCompleted(false);
//            setResult(RESULT_OK);
//            finish();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

//    @Override
//    public void onSensorChanged(SensorEvent event) {
//        long ts = 0;
//        if (started) {
//            Sensor sensor = event.sensor;
//            ts = System.currentTimeMillis();
//            if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
//                ax = event.values[0];
//                ay = event.values[1];
//                az = event.values[2];
//                long timestamp = System.currentTimeMillis();
//                data = new AccelerometerData(timestamp, ax, ay, az);
//                sensorData.add(data);
//            }
//            if (sensor.getType() == Sensor.TYPE_GYROSCOPE) {
//                gx = event.values[0];
//                gy = event.values[1];
//                gz = event.values[2];
//            }
//            if (sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
//                mx = event.values[0];
//                my = event.values[1];
//                mz = event.values[2];
//            }
//            if (sensor.getType() == Sensor.TYPE_ORIENTATION) {
//                ox = event.values[0];
//                oy = event.values[1];
//                oz = event.values[2];
//            }
//        } else {
//            allSensorData = new AllSensorData(ts, ax, ay, az, gx, gy, gz, mx, my, mz, ox, oy, oz);
//            allSensorDataArrayList.add(allSensorData);
//            Toast.makeText(getApplicationContext(), "Success", Toast.LENGTH_SHORT).show();
//        }
//    }
}
