package com.smartapps.main;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import model.AccelerometerData;
import model.SessionManager;
import model.TemplateGenerator;

/**
 * Created by Mohammad-Ghouri on 6/28/16.
 */
public class Testing3D extends Activity implements SensorEventListener {
    private TextView daycounter, testCounter;
    private SensorManager sensorManager;
    private AccelerometerData data;
    private ArrayList<AccelerometerData> sensorData;
    private boolean started = false;
    private LinearLayout gestureLayout;
    private ImageButton recordStatus;
    private File tempFile;
    private int initDate;
    private int currentDate;
    private int dateNum;
    private int maxDateNum;
    private int leftCount;
    private int currentTestCount;
    private String currentDateLong;
    protected int maxAbnormalBehaviors;
    private static final String KEY_NUM_ABNORMAL_BEHAVIORS = "num_abnormal_behaviors";
    private static final String KEY_NUM_FAILED_ATTEMPTS = "num_failed_attempts";
    protected int numAbnormalBehaviors;
    protected int numFailedAttempts;
    private SessionManager sessionManager;
    private SharedPreferences preferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().hide();
        setContentView(R.layout.testing);
        initGUI();
        sensorData = new ArrayList<>();
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        sessionManager = new SessionManager(getApplicationContext());
        preferences = getSharedPreferences(SessionManager.PREF_NAME, MODE_PRIVATE);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        if (prefs.getBoolean("firstrun", true)) {
            initDate = getInitDate();
            prefs.edit().putInt("initdate", initDate).commit();
            prefs.edit().putBoolean("firstrun", false).commit();
        }
        initDate = prefs.getInt("initdate", 0);
        dateNum = getDateNum(initDate);
        maxDateNum = getMaxDateNum();
        leftCount = getTestSizePerDay();
        currentTestCount = getInitTestCount();
        currentDateLong = getCurrentDate();
//        setDayTestTextView(dateNum, leftCount);
/////////////

        if (preferences.getInt(SessionManager.DATE_NUMBER, 0) == dateNum) {
            leftCount = preferences.getInt(SessionManager.LEFT_COUNT, 0);
            currentTestCount = preferences.getInt(SessionManager.CURRENT_TEST_COUNT, 0);
            setDayTestTextView(dateNum, leftCount);
            if (leftCount == 0) {
                gestureLayout.setEnabled(false);
                Toast.makeText(getApplicationContext(), "Today's Task is Completed", Toast.LENGTH_SHORT).show();
            }

        } else {
            setDayTestTextView(dateNum, leftCount);
            gestureLayout.setEnabled(true);
        }

/////////////

        maxAbnormalBehaviors = getMaxAbnormalBehaviors();
        if (savedInstanceState == null) {
            numAbnormalBehaviors = 0;
            numFailedAttempts = 0;
        } else {
            numAbnormalBehaviors = savedInstanceState.getInt(KEY_NUM_ABNORMAL_BEHAVIORS);
            numFailedAttempts = savedInstanceState.getInt(KEY_NUM_FAILED_ATTEMPTS);
        }


        gestureLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    recordStatus.setBackgroundResource(R.drawable.circel_green);
                    sensorData = new ArrayList<>();
                    started = true;
                    Sensor accelerometer = sensorManager
                            .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
                    sensorManager.registerListener(Testing3D.this, accelerometer,
                            SensorManager.SENSOR_DELAY_FASTEST);


                } else if (event.getAction() == MotionEvent.ACTION_UP) {
                    recordStatus.setBackgroundResource(R.drawable.circle_red);
                    started = false;
                    sensorManager.unregisterListener(Testing3D.this);
                    matchWithTemplate();

                }

                return true;
            }
        });


    }

    protected void setDayTestTextView(int dateNum, int leftCount) {
        daycounter.setText(String.valueOf(dateNum));
        testCounter.setText(String.valueOf(leftCount));
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_NUM_ABNORMAL_BEHAVIORS, numAbnormalBehaviors);
        outState.putInt(KEY_NUM_FAILED_ATTEMPTS, numFailedAttempts);
    }


    private void matchWithTemplate() {
        if (sensorData.size() > 0) {
            if (isBehaviorMatched(sensorData)) {
                // save this attempt with matched behavior into a success file
                saveSuccessAttempt(currentTestCount, currentDateLong);

                --leftCount;
                ++currentTestCount;
                sessionManager.saveTestDateAndCount(dateNum, leftCount, currentTestCount);
                if (leftCount == 0) {

                    if (dateNum == maxDateNum) {
                        Toast.makeText(getApplicationContext(), R.string.complete_all_testings, Toast.LENGTH_LONG).show();
                        onClearFailedAttempts();
                        onAllFinished();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.finish_oneday_task, Toast.LENGTH_LONG).show();
                        onClearFailedAttempts();
                        onOneDayFinished();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.successful_attempt, Toast.LENGTH_SHORT).show();

                    setDayTestTextView(dateNum, leftCount); // Add colors to mMessageText.setText(getString(R.string.draw_pattern_to_unlock, dateNum, leftCount));
                    onClearFailedAttempts();
                }
            } else {
                onAbnormalBehavior();
                if (numAbnormalBehaviors == maxAbnormalBehaviors) {
                    Toast.makeText(getApplicationContext(), R.string.max_failed_attempts, Toast.LENGTH_LONG).show();

                    // save this attempt with abnormal behavior into a failedtrial file
                    saveFailedAttempt(numAbnormalBehaviors, currentTestCount, currentDateLong);

                    --leftCount; // Skip this test - decrement the number of left attempts by 1
                    ++currentTestCount; // increment the current test number by 1
                    sessionManager.saveTestDateAndCount(dateNum, leftCount, currentTestCount);
                    /////My Trial=== if the user fails all the tests
                    if (leftCount == 0) {

                        if (dateNum == maxDateNum) {
                            Toast.makeText(getApplicationContext(), R.string.complete_all_testings, Toast.LENGTH_LONG).show();
                            onClearFailedAttempts();
                            onAllFinished();
                        } else {
                            Toast.makeText(getApplicationContext(), R.string.finish_oneday_task, Toast.LENGTH_LONG).show();
                            onClearFailedAttempts();
                            onOneDayFinished();
                        }
                    }

                    ///////////////////////////////////

                    setDayTestTextView(dateNum, leftCount);
                    onClearFailedAttempts();
                } else if (numAbnormalBehaviors == maxAbnormalBehaviors - 1) {
                    Toast.makeText(getApplicationContext(), R.string.behavior_failed_twice, Toast.LENGTH_LONG).show();

                    // save this attempt with abnormal behavior into a failedtrial file
                    saveFailedAttempt(numAbnormalBehaviors, currentTestCount, currentDateLong);
                    //sessionManager.saveTestDateAndCount(dateNum, leftCount, currentTestCount);
                } else {
                    Toast.makeText(getApplicationContext(), R.string.failed_attempt, Toast.LENGTH_LONG).show();
                    // save this attempt with abnormal behavior into a failedtrial file
                    saveFailedAttempt(numAbnormalBehaviors, currentTestCount, currentDateLong);
                    // sessionManager.saveTestDateAndCount(dateNum, leftCount, currentTestCount);
                }
            }
        } else {
            Toast.makeText(this, "No Recorded Gesture", Toast.LENGTH_SHORT).show();
        }

    }

    private void initGUI() {
        daycounter = (TextView) findViewById(R.id.dayCounter);
        testCounter = (TextView) findViewById(R.id.testCounter);
        gestureLayout = (LinearLayout) findViewById(R.id.gestureView);
        recordStatus = (ImageButton) findViewById(R.id.recordStatus);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (started) {


            double x = event.values[0];
            double y = event.values[1];
            double z = event.values[2];

            long timestamp = System.currentTimeMillis();
            data = new AccelerometerData(timestamp, x, y, z);
            sensorData.add(data);

        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    protected boolean isBehaviorMatched(ArrayList<AccelerometerData> sData) {


        StringBuilder sBuilder = new StringBuilder();
        if (sData.size() > 0) {
            for (int i = 0; i < sData.size(); i++) {
                sBuilder.append(sData.get(i).getX() + " " + sData.get(i).getY() + " " + sData.get(i).getZ() + "\n");
            }
            try {
                String storageState = Environment.getExternalStorageState();
                if (storageState.equals(Environment.MEDIA_MOUNTED)) {
                    File tempFileDir = this.getCacheDir();
                    tempFile = new File(tempFileDir, "tempBehaviorFile.txt");
                    FileOutputStream fos = new FileOutputStream(tempFile);
                    fos.write(sBuilder.toString().getBytes());
                    fos.close();
                } else {
                    Log.i("file saving problem", "unable to write to external storage");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Toast.makeText(this, "No Recorded Gesture...", Toast.LENGTH_LONG).show();
        }

        sData.clear();
        String mTemplateDataDir = this.getExternalFilesDir(null) + "/u" + String.valueOf(MenuActivity.userID) + "/template/";
        int templateNum = new File(mTemplateDataDir).listFiles().length;
        double[] testDists = new double[templateNum];
        String tempFilePath = tempFile.getAbsolutePath();
        for (int i = 0; i < templateNum; i++) {
            String templateFilePath = mTemplateDataDir + "template" + String.valueOf(i + 1) + ".txt";
            double testDist = TemplateGenerator.DtwDistance(tempFilePath, templateFilePath);
            testDists[i] = testDist;
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        double threshold = Double.longBitsToDouble(prefs.getLong("threshold", Double.doubleToLongBits(-1)));
        int count = 0;
        int majority = 3;
        for (double dist : testDists) {
            if (dist < threshold) {
                count++;
            }
        }
        boolean result;
        if (count >= majority) {
            result = true;
        } else {
            result = false;
        }
        return result;


    }

    protected void saveFailedAttempt(int numAbnormalBehaviors, int currentTestCount, String currentDateLong) {
        String mTestDataDir = this.getExternalFilesDir(null) + "/u" + String.valueOf(MenuActivity.userID) + "/test/" + currentDateLong + "/";
        String mTestFailedPath = mTestDataDir + "test" + String.valueOf(currentTestCount) + "_failedtrial" + String.valueOf(numAbnormalBehaviors) + ".txt";
        File mTestFailedFile = new File(mTestFailedPath);
        try {
            FileUtils.copyFile(tempFile, mTestFailedFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void saveSuccessAttempt(int currentTestCount, String currentDateLong) {
        String mTestDataDir = this.getExternalFilesDir(null) + "/u" + String.valueOf(MenuActivity.userID) + "/test/" + currentDateLong + "/";
        String mTestSuccessPath = mTestDataDir + "test" + String.valueOf(currentTestCount) + "_success.txt";
        File mTestSuccessFile = new File(mTestSuccessPath);
        try {
            FileUtils.copyFile(tempFile, mTestSuccessFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    protected void onAbnormalBehavior() {
        ++numAbnormalBehaviors;
        ++numFailedAttempts;
    }

    protected void onClearFailedAttempts() {
        numAbnormalBehaviors = 0;
        numFailedAttempts = 0;
    }

    protected void onCancel() {
        setResult(RESULT_CANCELED);
        finish();
    }

    protected void onForgotPattern() {
    }

    protected void onOneDayFinished() {
        setResult(RESULT_FIRST_USER);
        finish();
    }

    protected void onAllFinished() {
        setResult(RESULT_OK);
        finish();
    }

    protected int getInitDate() {
        Calendar cal = Calendar.getInstance();
        initDate = cal.get(Calendar.DAY_OF_YEAR);
        return initDate;
    }

    protected int getDateNum(int initDate) {
        Calendar cal = Calendar.getInstance();
        currentDate = cal.get(Calendar.DAY_OF_YEAR);
        if (currentDate == initDate) {
            dateNum = 1;
        } else if (currentDate - initDate == 1) {
            dateNum = 2;
        } else if (currentDate - initDate == 2) {
            dateNum = 3;
        } else if (currentDate - initDate == 3) {
            dateNum = 4;
        } else if (currentDate - initDate == 4) {
            dateNum = 5;
        } else {
            Toast.makeText(getApplicationContext(), R.string.max_test_attempts, Toast.LENGTH_LONG).show();
        }
        return dateNum;
    }

    protected int getMaxDateNum() {
        return 5;
    }

    protected int getTestSizePerDay() {
        return 15;
    }

    protected int getInitTestCount() {
        return 1;
    }

    protected String getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        return sdf.format(cal.getTime());
    }


    protected int getMaxAbnormalBehaviors() {
        return 3;
    }
}

