package com.smartapps.main;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import java.util.Calendar;
import java.util.Date;

import model.SessionManager;

public class MenuActivity extends Activity implements OnClickListener {
    private Button training, testing, summary, video;
    public static int userID = 1;
    private DateChangeListener mDateChangeListener;
    private static IntentFilter mIntentFilter;
    private static final int REQ_SET_PATTERN = 1;
    private static final int REQ_VERIFY_PATTERN = 2;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActionBar().hide();
        setContentView(R.layout.newmenu);
        initGUI();
        sessionManager = new SessionManager(getApplicationContext());
        if (!sessionManager.hasTrainingCompleted()) {
            training.setEnabled(false);
            training.setTextColor(getResources().getColor(R.color.lbl_name, null));
            testing.setEnabled(true);
            testing.setTextColor(getResources().getColor(R.color.white, null));
        }

        startListeningDateChange();
        handleNotification();

        training.setOnClickListener(this);
        testing.setOnClickListener(this);
        summary.setOnClickListener(this);
        video.setOnClickListener(this);

    }

    private void initGUI() {
        training = (Button) findViewById(R.id.trainingbutton);
        testing = (Button) findViewById(R.id.testingButton);
        summary = (Button) findViewById(R.id.summaryButton);
        video = (Button) findViewById(R.id.videobutton);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.trainingbutton:
                Intent fingerP = new Intent(MenuActivity.this, TrainingClass.class);
                startActivityForResult(fingerP, REQ_SET_PATTERN);
                break;
            case R.id.testingButton:
                Intent free3d = new Intent(MenuActivity.this, Testing3D.class);
                startActivityForResult(free3d, REQ_VERIFY_PATTERN);

                break;
            case R.id.summaryButton:
                Intent summ = new Intent(MenuActivity.this, SummaryClass.class);
                startActivity(summ);
                break;
            case R.id.videobutton:
                Intent vdo = new Intent(MenuActivity.this, VideoClass.class);
                startActivity(vdo);
                break;

            default:
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {

            case REQ_SET_PATTERN: {
                if (resultCode == RESULT_OK) {
                    training.setEnabled(false);
                    training.setTextColor(getResources().getColor(R.color.lbl_name, null));
                    testing.setEnabled(true);
                    testing.setTextColor(getResources().getColor(R.color.white, null));
                }
                break;
            }
            case REQ_VERIFY_PATTERN: {
                if (resultCode == RESULT_OK) {
//                    testing.setEnabled(false);
//                    testing.setTextColor(getResources().getColor(R.color.lbl_name, null));
                    unregisterReceiver(mDateChangeListener);
                } else if (resultCode == RESULT_FIRST_USER) {
                    // don't allow users to access the testing page after finishing one day's task
//                    testing.setEnabled(false);
//                    testing.setTextColor(getResources().getColor(R.color.lbl_name, null));
                }
            }
        }
    }

    private void startListeningDateChange() {
        mDateChangeListener = new DateChangeListener();
        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(Intent.ACTION_DATE_CHANGED);
        registerReceiver(mDateChangeListener, mIntentFilter);
    }

    private class DateChangeListener extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (action.equals(Intent.ACTION_DATE_CHANGED)) {
//                testing.setEnabled(true);
//                testing.setTextColor(getResources().getColor(R.color.white, null));
            }
        }
    }

    private void handleNotification() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 10);
        calendar.set(Calendar.MINUTE, 45);
        calendar.set(Calendar.SECOND, 0);
        if ((calendar.getTime().compareTo(new Date()) < 0)) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        Intent alarmIntent = new Intent(this, AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, pendingIntent);
    }

    public static class AlarmReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.drawable.logonew)
                    .setContentTitle("Motion Authentication")
                    .setContentText("Touch to see if you have finished today's task")
                    .setAutoCancel(true)
                    .setSound(alarmSound);
            Intent resultIntent = new Intent(context, SummaryClass.class);
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(MenuActivity.class);
            stackBuilder.addNextIntent(resultIntent);
            PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            mBuilder.setContentIntent(resultPendingIntent);
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(1, mBuilder.build());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(mDateChangeListener);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
    }

}
